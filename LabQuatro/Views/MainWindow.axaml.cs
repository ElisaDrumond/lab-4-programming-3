using System;
using System.IO;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Markup.Xaml;
using NAudio.Wave;

namespace LabQuatro.Views
{
    public partial class MainWindow : Window
    {
        private AudioFileReader? audioReader;
        private WaveOutEvent? waveOut;

        private WaveOutEvent? additionalWaveOut;
        private AudioFileReader? additionalAudioReader;
        
        private double initialBottom;
        private double posicaoFinalX = 970; 
        
        private Canvas? game;
        private Avalonia.Controls.Image sprite;

        public MainWindow()
        {
            InitializeComponent();
            game = this.FindControl<Canvas>("Game");
            sprite = this.FindControl<Avalonia.Controls.Image>("Mario") ?? throw new InvalidOperationException("Element 'Mario' not found in XAML");;
            initialBottom = Canvas.GetBottom(sprite);
            this.KeyDown += Game_KeyDown;

            string musicThemePath = "C:/Users/Elisa/Projetos/Avalonia/LabQuatro/LabQuatro/Assets/super-mario-theme.mp3";
            audioReader = InitializeAudio(musicThemePath);
            
            if (audioReader != null)
            {
                waveOut = new WaveOutEvent();
                waveOut.Init(audioReader);
                waveOut.Volume = 0.3f;
                waveOut.Play();
                
                Closing += async (sender, args) =>
                {
                    await Task.Run(() =>
                    {
                        waveOut.Stop();
                        waveOut.Dispose(); 
                    });
                };
            }
        }

        private AudioFileReader? InitializeAudio(string musicThemePath)
        {
            if (File.Exists(musicThemePath))
            {
                return new AudioFileReader(musicThemePath);
            }
            else
            {
                return null;
            }
        }
        
        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

      
        private void Game_KeyDown(object? sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Right:
                    MoveRight();
                    break;
                case Key.Left:
                    MoveLeft();
                    break;
                case Key.Up:
                    Fly();
                    break;
                case Key.Down:
                    MoveBottom();
                    break;
            }
        }

        private void MoveRight()
        {
            double spriteWidth = sprite.Width;
            double newPositionX = Canvas.GetLeft(sprite) + 5;
            double rightLimit = 1050 - spriteWidth;
            
            if (newPositionX <= rightLimit)
            {
                Canvas.SetLeft(sprite, newPositionX);
            }

            UpdateSpritePosition();
        }

        private void MoveLeft()
        {
            double newPositionX = Canvas.GetLeft(sprite) - 5;
            double leftLimit = 0;
        
            if (newPositionX >= leftLimit)
            {
                Canvas.SetLeft(sprite, newPositionX);
            }
            
            UpdateSpritePosition();
        }

        private async void Fly()
        {
            PlayAdditionalSound("C:/Users/Elisa/Projetos/Avalonia/LabQuatro/LabQuatro/Assets/hq-explosition.mp3");

            sprite.Source = new Avalonia.Media.Imaging.Bitmap("Assets/images/jetpack.png");
            sprite.Width = 100;
            Canvas.SetTop(sprite, 300);

            await Task.Delay(550);
            additionalWaveOut?.Stop();
            additionalWaveOut?.Dispose();
        }

        private async void MoveBottom() {

            PlayAdditionalSound("C:/Users/Elisa/Projetos/Avalonia/LabQuatro/LabQuatro/Assets/box-crash.mp3");
          
            sprite.Source = new Avalonia.Media.Imaging.Bitmap("Assets/images/mario.png");
            sprite.Width = 65;  
            Canvas.SetTop(sprite, 450);

            await Task.Delay(700);
            additionalWaveOut?.Stop();
            additionalWaveOut?.Dispose();
        }

        private void PlayAdditionalSound(string path) 
        {
            string audioPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path);
            additionalAudioReader = InitializeAudio(audioPath);

            if (additionalAudioReader != null)
            {
                additionalWaveOut = new WaveOutEvent();
                additionalWaveOut.Init(additionalAudioReader);
                additionalWaveOut.Play();
            }
        }

        private void UpdateSpritePosition()
        {
            if (game == null) return;
                double spriteX = Canvas.GetLeft(sprite);
            
            double inferiorLimit = game.Height - sprite.Height;

                if (spriteX >= posicaoFinalX)
                {
                    EndGame();
                    return;
                }
        }

        private void EndGame()
        {
            Close();
        }
    }
}